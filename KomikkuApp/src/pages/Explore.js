import React,{useEffect, useState} from 'react'
import Axios from 'axios'
import { StyleSheet, Text, View,TouchableOpacity, Touchable, Image, TextInput } from 'react-native'
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { AntDesign } from '@expo/vector-icons';


const Explore = ({navigation}) => {
    const [items, setItems] = useState([])

    useEffect(() => {
        // getData();
        Axios.get('https://mangamint.kaedenoki.net/api/manga/page/2')
    
        .then(res => {
            // console.log('res get data: ', res)
            setItems(res.data.manga_list)
        }).catch(err =>{
            console.log(err)
        })

    }, [])

    return (
        <View style={{backgroundColor: '#ffff', flex:1}}>
        <ScrollView>

        <View style={styles.content}>
        <View style={styles.searchBar}>
            <View style={{paddingVertical:2, flexDirection: 'row'}}>

        <AntDesign style={{marginRight: 5}}
        name="search1" size={15} color="#827878" />
            <TextInput 
            placeholder='Cari Komik...'
                
              />
            </View>

        </View>
            <View style={{flexDirection: 'row', marginVertical: 27, justifyContent:'space-between'}}>
                <TouchableOpacity  style={styles.type}>
                    <Text style={{color: '#ffff', fontWeight: 'bold'}}>Semua</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.type}>
                    <Text style={{color: '#ffff', fontWeight: 'bold'}}>Manga</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.type}>
                    <Text style={{color: '#ffff', fontWeight: 'bold'}}>Manhwa</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.type}>
                    <Text style={{color: '#ffff', fontWeight: 'bold'}}>Manhua</Text>
                </TouchableOpacity>
            </View>
            
            <View style={{marginBottom: 50}} >
            <FlatList
                data={items}
                renderItem={({item}) => {
                       return (
                    <TouchableOpacity 
                    onPress={() => navigation.navigate('Detail', {itemId : item.endpoint})}
                    style={styles.card}>
                        <Image 
                        style={styles.cover}
                        source={{uri:item.thumb}}/>
                        <View style={{marginHorizontal: 15, marginVertical: 12, justifyContent: 'space-between'}}>
                            <Text style={styles.title}>{item.title}</Text>
                            <Text style={{color: '#7E69CC', lineHeight: 20, marginBottom:3}}>{item.type}</Text>
                            <Text style={styles.chap}>{item.chapter}</Text>
                            <Text style={{color: '#7C739E', fontWeight: '500'}}>Update {item.updated_on} lalu</Text>
                        </View>
                    </TouchableOpacity>
                       )
                }}
            />
            </View>

        </View>
        </ScrollView>
        </View>
    )
}

export default Explore

const styles = StyleSheet.create({
    content : {
        marginVertical: 40,
        marginHorizontal:32
    },
    searchBar :{
        borderWidth:0, 
        backgroundColor: '#E4E4E4', 
        height: 33,
        borderRadius: 20, 
        paddingVertical: 5,
        paddingHorizontal: 15,
        flexDirection: 'row'
    },

    type : {
        borderWidth: 0,
        backgroundColor: '#7E69CC',
        height: 32,
        width: 70,
        borderRadius: 15,
        alignItems : 'center',
        justifyContent: 'center',
        elevation: 2,
        marginBottom:5
    },
    cover : {
        height: 100,
        width: 115,
        borderRadius: 9
    },
    card : {
        backgroundColor: '#ffff',
        borderWidth: 0,
        borderRadius: 9,
        maxHeight: 102,
        flexDirection: 'row',
        marginBottom: 20,
        elevation:4
    },
    title : {
        fontSize: 17,
        fontWeight: 'bold',
        lineHeight: 19,
        marginVertical:4
    },
    chap : {
        fontWeight: 'bold',
        color: '#38394B',
        lineHeight:16,
        marginBottom: 3
    }
})
