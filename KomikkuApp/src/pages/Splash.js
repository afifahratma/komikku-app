import React,{useEffect} from 'react'
import { StyleSheet, Text, View,Image, StatusBar } from 'react-native'

const Splash = ({navigation}) => {
        useEffect(() => {
        setTimeout(() => {
            navigation.replace('SignUp')
        }, 3000)
    })
    return (
        <View style={styles.container}>

        <View style={styles.logo}>
            <Image 
            
            source={require('../asset/logo.png')}/>
           
        </View>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container : {
        backgroundColor: '#38394B', 
        flex:1
    },
    logo : {
        alignItems: 'center', 
        backgroundColor: '#38394B', 
        justifyContent: 'center',
        position: 'relative', 
        flex: 3
    }
})
