import React,{useState} from 'react'
import { StyleSheet, Text, View, Image, TextInput, Touchable, TouchableOpacity} from 'react-native'

import * as firebase from 'firebase'

const Login = ({navigation}) => {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const  firebaseConfig = {
        apiKey: "AIzaSyCl3M3jQ0ob1vsG9_9QwaLEzUuyUWONHqE",
        authDomain: "authfirebasern-d93e8.firebaseapp.com",
        projectId: "authfirebasern-d93e8",
        storageBucket: "authfirebasern-d93e8.appspot.com",
        messagingSenderId: "712004505929",
        appId: "1:712004505929:web:04efc74f57b2001d174a98"
      };
      // Initialize Firebase
      if(!firebase.apps.length){
          firebase.initializeApp(firebaseConfig);

      }
    
    const submit = () => {
        const data = {
            email, password
        }
        // console.log(data)
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(() => {
            navigation.navigate("MainApp")
            setEmail("")
            setPassword("")
        }) .catch(() => {
            
            alert('Email / Password Salah!')
            setEmail("")
            setPassword("")
        })
    }

    return (
        <View style={styles.container}>
            <View style={styles.imageWrapper}>
                <Image 
                style={{height: 96, width: 111}}
                source={require('../asset/logo.png')}/>

            </View>
            <View style={styles.formWrap}>
                <View style={styles.form}>
                    <Text style={{fontSize: 46, fontWeight: '700'}}>Login</Text>
                    <View style={{marginTop: 40}}>
                        <TextInput style={styles.inputBar}
                        value={email}
                        onChangeText={(value) => setEmail(value)}
                        placeholder='Email'/>
                        <TextInput style={styles.inputBar}
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                        secureTextEntry={true}
                        placeholder='Password'/>
                    </View>
                <View style={{alignItems: 'center', marginTop:90}}>
                    <TouchableOpacity 
                    onPress={submit}
                    style={styles.buttonAtas}>
                    <Text style={{
                        fontWeight: '700',
                        color: 'white',
                        fontSize : 20,
                        
                    }}>Login</Text>
                  </TouchableOpacity>
                  <Text style={{fontSize: 17, color: '#B8B2B2'}}>atau</Text>
                  <TouchableOpacity 
                  onPress={() => navigation.navigate('SignUp')}
                  style={styles.buttonBawah}>
                    <Text style={{
                        fontWeight: '700',
                        color: '#7E69CC',
                        fontSize : 20,
                        
                    }}>Sign Up</Text>
                  </TouchableOpacity>
                </View>
                </View>
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container : {
        backgroundColor: '#38394B', 
        flex:1
    },
    imageWrapper : {
        alignItems : 'center',
        justifyContent : 'center',
        height: 215,
        width: 411,
    },
    formWrap : {
        flex: 1,
        backgroundColor: '#FCFEFF',
        borderWidth:0,
        borderTopRightRadius: 35,
        borderTopLeftRadius: 35
    },
    form : {
        marginVertical: 32,
        marginHorizontal: 27
    },
    inputBar : {
        borderBottomWidth :1,
        borderBottomColor: '#B8B2B2',
        height: 36,
        fontSize: 18,
        marginBottom: 25,
        paddingHorizontal: 10
    },
    buttonAtas : {
        borderRadius : 15,
        borderWidth: 0,
        backgroundColor : '#7E69CC',
        width: 304,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 18
    },
    buttonBawah : {
        borderRadius : 15,
        borderWidth: 1,
        borderColor : '#7E69CC',
        width: 304,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 18
    }
})
