import React,  { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight } from 'react-native'
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { AntDesign } from '@expo/vector-icons';
import Axios from 'axios';


const Home = ({navigation}) => {
    const [populer, setPopuler] = useState([]);    
    const [genre, setGenre] = useState([]);
    const [latest, setLatest] = useState([]);

    useEffect(() => {
        Axios.get('https://mangamint.kaedenoki.net/api/manga/popular/1')
    
        .then(res => {
            // console.log('res get data: ', res)
            setPopuler(res.data.manga_list)
            // console.log(populer)
        }).catch(err =>{
            console.log(err)
        })
        Axios.get('https://mangamint.kaedenoki.net/api/genres')
        
            .then(res => {
                // console.log('res get data: ', res)
                setGenre(res.data.list_genre)
              
            }).catch(err => {
                console.log(err)
            })
        
        Axios.get('https://mangamint.kaedenoki.net/api/manga/page/1')
        
            .then(res => {
                // console.log('res get data: ', res)
                setLatest(res.data.manga_list)
            }).catch(err => {
                console.log(err)
            })
        

    }, [])

    return (
        <View style={{backgroundColor: '#ffff', flex:1}}>
        <ScrollView>
        <View style={styles.wrapper}>
           <View style={styles.populer}>
                <View style={styles.headlineWrap}>
                <View style={{flex:1}}>
                     <Text style={styles.headline}>Populer</Text>

                </View>
                <View style={{position: 'absolute',right: 0, flex:1}}>
                    <TouchableOpacity style={{borderWidth:0, backgroundColor: '#7E69CC', height: 28, width: 52, borderRadius: 20, alignItems: 'center', justifyContent: 'center'}}>
                    <AntDesign 

                    name="search1" size={20} color="white" />
                    </TouchableOpacity>

                </View>

                </View>
               <FlatList 
                    horizontal={true}
                   data={populer}
                   renderItem={({item}) => {
                       return (
                        <View>
                        <TouchableOpacity 
                        onPress={() => navigation.navigate('Detail', {itemId : item.endpoint})}
                        style={styles.populerList}>
                            <Image 
                            style={styles.cover} source={{uri : item.thumb}} />
                            <View style={styles.descKomik}>
                                <Text style={{color: '#7E69CC', lineHeight: 20}}>{item.type}</Text>
                                <Text numberOfLines={3} 
                                style={{fontWeight: 'bold', fontSize: 14, color: 'black', lineHeight: 18}}>
                                {item.title}</Text>
                            </View>
                        </TouchableOpacity>

                        </View>

                       )
                   }}
                   
                   keyExtractor={(item, index) => index.toString()}
               />
            </View>

            <View style={{marginBottom:17}}>
                   <View style={styles.headlineWrap}>
                        <Text style={styles.headline}>Kategori</Text>
                   </View>
                 
                    <FlatList
                    horizontal={true}
                    data={genre}
                    renderItem={({item}) => {
                        return (
                                <View style={styles.genre}>
                                    <Text style={{fontWeight: 'bold', color: '#38394B'}}>{item.genre_name}</Text>
                                </View>
                            
                        )
                    }}
                    
                    keyExtractor={(item, index) => index.toString()}
                    />

            
            </View>

                <View>
                   <View style={styles.headlineWrap}>
                    <Text style={styles.headline}>Terbaru</Text>

                   </View>
                   <FlatList 
                   data={latest}
                   numColumns={3}
                   renderItem={({item}) => {
                       return (
                           <View style={{paddingRight: 10, paddingBottom:10}}>

                            <TouchableOpacity 
                            onPress={() => navigation.navigate('Detail', {itemId : item.endpoint})}
                            style={styles.terbaruList}>
                                <Image style={styles.cover2} 
                                        source={{uri : item.thumb}} />
                                        <View style={styles.descKomik}>
                                            <Text numberOfLines={2} 
                                            style={{fontWeight: 'bold', fontSize: 14, color: 'black'}}>
                                            {item.title}</Text>
                                            <Text style={{color: '#7E69CC', fontWeight: '700'}}>{item.chapter}</Text>
                                            <Text style={{color: '#7C739E', fontWeight: '600',fontSize: 13}}>Update {item.updated_on} lalu</Text>
                                        </View>
                                </TouchableOpacity>
                           </View>
                       )
                   }}
                     keyExtractor={(item, index) => index.toString()}
                />
                </View>

            </View>
        </ScrollView>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    populer : {
        backgroundColor: '#ffff',
        marginVertical: 17
        // flex: 1,
        // flexDirection: 'row'
    },
    wrapper : {
        marginHorizontal: 22,
        marginVertical: 20,
    
    },
    headlineWrap : {flexDirection: 'row', marginVertical: 17},
    headline : {
        fontSize: 26, 
        fontWeight: 'bold',
        lineHeight: 33,
      
    },
    populerList : {
        borderWidth: 1,
        borderColor: '#E9EDF0',
        borderRadius : 13,
        height: 218,
        width: 120,
        marginRight: 10
     
    },
    cover : {
        width: 120, 
        height: 142,
        borderWidth: 1,
        borderRadius : 13,
    },
    cover2 : {
        width: 114, 
        height: 142,
        borderWidth: 1,
        borderRadius : 13,
    },
    descKomik : {
        paddingHorizontal: 5,
        paddingVertical: 5,
        justifyContent: 'space-around'
    },
    genre : {
        borderWidth: 0,
        backgroundColor: '#83D1FE',
        height: 32,
        width: 92,
        borderRadius: 9,
        alignItems : 'center',
        justifyContent: 'center',
        elevation: 2,
        marginBottom:5,
        marginRight: 8
    },
    terbaruList : {
        borderWidth: 1,
        borderColor: '#E9EDF0',
        borderRadius : 13,
        height: 210,
        width: 115,
      
     
    },
})
