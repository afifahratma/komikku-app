import React,{useEffect, useState} from 'react'
import Axios from 'axios'
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'

const Chapter = ({route}) => {
    const { itemChapter} = route.params;

    const [items, setItems] = useState([])
    useEffect(() => {
        Axios.get(`https://mangamint.kaedenoki.net/api/chapter/${itemChapter}`)
    
        .then(res => {
            // console.log('res get data: ', res)
            console.log(res.data.chapter_image)
            setItems(res.data.chapter_image)
        }).catch(err =>{
            console.log(err)
        })
    },  [])
    return (


        <View style={styles.container}>
        <Text>{items.chapter_pages}</Text>
            <FlatList
            style={{marginBottom: 4}}
            data={items}
            renderItem={({item}) => {
                    return (
                        <View>
                            <Image style={{width: 300, height:400}}
                            source={{uri : item.chapter_image_link}}/>
                        </View>
                       )
            }}
            />
 
        </View>
     
    )
}

export default Chapter

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex:1,
        marginBottom:10,
        backgroundColor: 'white'
    }
})
