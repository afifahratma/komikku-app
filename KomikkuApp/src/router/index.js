import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {createStackNavigator} from '@react-navigation/stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {NavigationContainer} from '@react-navigation/native'
import { Ionicons } from '@expo/vector-icons';

import SignUp from '../pages/Signup'
import Login from '../pages/Login'
import Detail from '../component/Detail'
import Home from '../pages/Home'
import Profile from '../pages/Profile'
import Explore from '../pages/Explore'
import Splash from '../pages/Splash'
import Chapter from '../component/Chapter'

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Router() {
    
    return (
        <NavigationContainer>
            <Stack.Navigator>

                <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }}/>
                <Stack.Screen name="SignUp" component={SignUp} options={{ headerShown: false }}/>
                <Stack.Screen name="Login" component={Login} options={{ headerShown: false }}/>
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }}/>
                <Stack.Screen name="Detail" component={Detail} />
                <Stack.Screen name="Chapter" component={Chapter} />
                <Stack.Screen name="Profile" component={Profile} />
                <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }}/>
                {/* <Stack.Screen name="MyDrawwer" component={MyDrawwer}/> */}
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => (
        <Tab.Navigator
         screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            if (route.name === 'Home') {
              return (
                <Ionicons
                  name={
                    focused
                      ? 'ios-home-sharp'
                      : 'ios-home-outline'
                  }
                  size={size}
                  color={color}
                />
              );
            } else if (route.name === 'Explore') {
              return (
                <Ionicons
                  name={focused ? 'book-sharp' : 'book-outline'}
                  size={size}
                  color={color}
                />
              );
            } else {
                return (
                <Ionicons
                  name={focused ? 'ios-person-circle-sharp' : 'ios-person-circle-outline'}
                  size={size}
                  color={color}
                />
              );
            }
          },
        })}
        tabBarOptions={{
          activeTintColor: '#7E69CC',
          inactiveTintColor: 'gray',
        }}>
            <Tab.Screen name="Home" component={Home} />
            <Tab.Screen name="Explore" component={Explore}/>
            <Tab.Screen name="Profile" component={Profile}/>
        </Tab.Navigator>
    
)
